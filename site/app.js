const http = require('http')
const fs = require('fs')
const cmd = require('node-cmd')

const server = http.createServer((req, res) => {
    if(req.url === "/"){
        fs.readFile("./index.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    }
    else if(req.url.match("rob.jpg$")){
        cmd.runSync("./newrob.py -o rob.jpg");
        var fileStream = fs.createReadStream("./rob.jpg");
        res.writeHead(200, {"Content-Type": "image/jpg"});
        fileStream.pipe(res);
    }
    // res.writeHead(200,{'content-type':'image/jpg'})
    // fs.createReadStream('./rob.jpg').pipe(res)
    // res.writeHead(200, { 'content-type': 'text/html'})
    // fs.createReadStream('index.html').pipe(res);
})

server.listen(6969)
