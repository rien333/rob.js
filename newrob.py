#!/usr/bin/python3

from PIL import Image, ImageOps, ImageFont, ImageDraw, ImageEnhance
import math
from glob import glob
import random as rnd
import argparse

class WaveDeformer:

    def transform(self, x, y):
        y = y + self.a*math.sin(x/self.d)
        return x, y

    def transform_rectangle(self, x0, y0, x1, y1):
        return (*self.transform(x0, y0),
                *self.transform(x0, y1),
                *self.transform(x1, y1),
                *self.transform(x1, y0),
                )
    
    def getmesh(self, img):
        self.w, self.h = img.size
        gridspace = rnd.randint(20,40)
        self.a = rnd.uniform(10, 25) if rnd.random() > 0.8 else 10
        self.d = rnd.uniform(10, 90) if rnd.random() > 0.0 else 40

        target_grid = []
        for x in range(0, self.w, gridspace):
            for y in range(0, self.h, gridspace):
                target_grid.append((x, y, x + gridspace, y + gridspace))

        source_grid = [self.transform_rectangle(*rect) for rect in target_grid]

        return [t for t in zip(target_grid, source_grid)]


def random_op(im, op, prob, *args):
    if rnd.random() > prob:
        return op(im, *args)
    return im

def random_enhancer(im, op, prob, *args):
    # the enhance module works different from the ops module
    if rnd.random() > prob:
        enhancer = op(im)
        # print(enhancer)
        return enhancer.enhance(*args)
    return im

parser = argparse.ArgumentParser(description='Generate a rob image')
parser.add_argument('--output', '-o', type=str, metavar="path", 
                    help='File to write rob image to')

deformer = WaveDeformer()
args = parser.parse_args()
ops = []

if rnd.random() > 0.01:
    files = glob("ims/*jpg")
    f = rnd.choice(files) 
else:
    f = "ims/DK.png"
    ops = [(ImageOps.autocontrast, 0.61, rnd.randint(0, 59)), 
            (ImageOps.deform, 0.2, deformer),]

im = Image.open(f)


if rnd.random() > 0.7:
    im = ImageOps.crop(im, (rnd.randint(0, 120), rnd.randint(0, 120), rnd.randint(0, 120), rnd.randint(0, 120)))
    im = ImageOps.scale(im, 1.2+rnd.random(), resample=rnd.choice([Image.Resampling.NEAREST, Image.Resampling.BOX]))

if rnd.random() > 0.5:
    im = ImageOps.mirror(im)

ops += [(ImageOps.autocontrast, 0.61, rnd.randint(0, 59)), 
       (ImageOps.invert, 0.76),
       (ImageOps.invert, 0.8),
       (ImageOps.deform, 0.72, deformer),
       (ImageOps.deform, 0.9, deformer),
       (ImageEnhance.Color, 0.8, rnd.uniform(0.75, 4)),
       (ImageEnhance.Contrast, 0.72, rnd.uniform(0.8, 3.9)),
       (ImageEnhance.Brightness, 0.8, rnd.uniform(0.75, 2.8)),
       (ImageEnhance.Sharpness, 0.75, rnd.uniform(0.85, 7.0)),
       (ImageOps.solarize, 0.56, rnd.randint(0, 163)),
       (ImageOps.posterize, 0.56, rnd.randint(1,6)),
       (ImageOps.grayscale, 0.88)]

rnd.shuffle(ops)

for o in ops:
    f = random_enhancer if str(o[0]).startswith("<class 'PIL.ImageEnhance") else random_op
    im = f(im, *o)

if rnd.random() > 0.4:
    font = ImageFont.truetype("fonts/" +
                              rnd.choice(["Monaco_Linux.ttf", "AlteHaasGroteskBold.ttf",
                                          "impact.ttf",  "Symbola.ttf", "DejaVuSans-Bold.ttf"]), 
                              rnd.randint(69, 260)) # fontsize
    draw = ImageDraw.Draw(im)
    color = None
    while not color:
        try:
            color = (rnd.randint(1, 254), rnd.randint(1, 254), rnd.randint(1, 254))
            draw.text( (rnd.randint(0, im.size[0]//2.5), rnd.randint(0, im.size[1]//4)), 
                       rnd.choice(["welkom in\n de kelder", "schrobben", "poetsen", "wet floors",
                                   "rob geus", "lotte", "danny"]),
                       color, font=font)
        except:
            continue
        break

try:
    if rnd.random() > 0.5:
        im = ImageOps.scale(im, 1.1+rnd.random(), resample=rnd.choice([Image.Resampling.NEAREST, Image.Resampling.BOX]))
except:
    pass

# import torch
# import torchaudio
# import numpy as np

# arr = np.array(im) # im2arr.shape: height x width x channel
# o, _ = torchaudio.sox_effects.apply_effects_tensor(
#     torch.from_numpy(im).view(1, im.width*im.height*3), 16000, [effect])

# im = Image.fromarray(o[0][:im.width*im.height*3].numpy())

im.save(args.output if args.output else "r.jpg", quality=rnd.randint(1, 69))

