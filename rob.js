#!/bin/node

// # TODO 
// - add a 'help' command with task descriptions
// 	- Denk dat dit met een regex moet if (txt.match("help "))
// - fish eye lens effect 
// 	- seems to be possible through the deformer class

const chrono = require('chrono-node');
const util = require('util');
const fs = require('fs');

var stats_path = "stats.json";
let users = ['liza', 'iarina', 'klaas', 'lotte', 'joris'];
let tasks = ['kitchen', 'vacuuming', 'toilets', 'bathroom', 'trash'];

function choice(choices) {
  var index = Math.floor(Math.random() * choices.length);
  return choices[index];
}

function arrayRotate(arr, count) {
  count -= arr.length * Math.floor(count / arr.length);
  arr.push.apply(arr, arr.splice(0, count));
  return arr;
}

Date.prototype.getWeek = function (dowOffset) {
    dowOffset = typeof(dowOffset) == 'number' ? dowOffset : 1;
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};

function task(user, date) {
    const u = user.toLowerCase();
    const n = tasks.length;
    // 3 is an offset
    const r = arrayRotate([...tasks], ((date.getWeek() % n)*-1) +3);
    // users and tasks are a mapping
    const idx = users.indexOf(u);
    return r[idx];
}

function dweilen(date) {
    const n = users.length;
    // offset by four for modulation
    const idx = (date.getMonth() + 4) % n
    return users[idx];
}

function checkdate(txt) {
    // make sure date starts on monday
    const l = chrono.en.parse(txt);
    if (l.length == 0) { // no dates found
        return chrono.en.parse('today')[0]; 
    }
    else { 
        // chrono gets confused about wether weeks start on sunday/monday
        // See: https://github.com/wansit/chrono
        var d = l[0];
        if (d.text === "this week") {
            d.start.assign('day', d.start.get('day') + 1);
            return d;
        }
        else {
            return d;
        }
    }
}

function progressTable() {
    const d = new Date();
    const w = d.getWeek();
    var t = '';
    var t_header = d.toLocaleDateString() + "\n";
    t_header += "———————————————————\n";
    row = "*%s*:\t\t%s\t\t%s\n";

    for (var u of users) {
        var name = u[0].toUpperCase()+u.slice(1);
        const done = stats[w][u];
        // modulate using '+2'
        t += util.format(row, name, task(u, d), 
                         done ? "✅" : "❌");
    }
    var dweiler = dweilen(d)
    dweiler = dweiler[0].toUpperCase()+dweiler.slice(1);
    t += util.format(row, "Mopping", dweiler, stats[w]["dweilen"] ? "✅" : "❌");
    return t_header + t;
}

function roblogic(txt, sender) {
    const d = checkdate(txt);
    let header = ``;
    var reply;

    
    if (['gedweild', 'dweilen done', 'moped', 'mopped'].some(t => txt.includes(t))) {
        const done = updateStats('dweilen');
        if (done) {
            reply = ": mopped ✅";
        }
        else {
            reply = "Je kan zelf wel eens een dweilbeurt gebruiken jij zwabber ❌";
        }
    }
    else if (['✅', 'genomen', 'did my', 'did the', 'done', 'gedaan', 'geneukt', 'gezogen', 'check'].some(t => txt.includes(t))) {
        const done = updateStats(sender);
        if (done){
            reply = sender+": " + task(sender, new Date()) + " ✅";
        }
        else {
            reply = "Heyoo even dimmen gast"
            console.log(sender)
        }
    }
    else if (sender.toLowerCase() === "wander") {
        reply = "fak op wander"
    }
    else if (users.some(u => txt.includes(u))) {
        for (var u of users) {
            if (txt.includes(u)) {
                break;
            }
        }
        // if today
        if (d.date().toDateString() == new Date().toDateString()) {
            reply = task(u, d.date());
        }
        else {
            var t = task(u, d.date());
            var name = u[0].toUpperCase() + u.slice(1);
            reply = d.text + " should " + name + " " + t;
        }
    }
    else if (txt.includes('readstats')) {
        reply = ""
        stats = readStats()
    }
    else if (txt.includes('stats')) {
        reply = printStats()
    }
    else if (txt.includes('I')) {
        if (d.date().toDateString() == new Date().toDateString()) {
            reply = task(sender, d.date());
        }
        else {
            const t = task(sender, d.date());
            reply = d.text + " you have to " + t;
        }
    }
    else if (txt.includes('help')) {
		if (txt.endsWith('afnemen')) {
            reply = "Fornuis, keukenblad, tafel in de woonkamer, vensterbanken, schoenenbakkie strelen";
        }
        else if (txt.endsWith('stofzuigen')) {
            reply = "Als je boven niet doet kan je het net zo goed niet doen";
        }
        else if (txt.endsWith('wc')) {
            reply = "Het is vrij ranzig maar pak alles, ook onder de bril enzo";
        }
        else if (txt.endsWith('badkamer')) {
            reply = "Een dag zonder chloor is een dag niet geleefd, gebruik ontkalker voor de kranen mochten die vies zijn.   Klop de badmat uit/doe hem in de was. Doe ook de doekjes enzo even wassen chillll";
        }
        else if (txt.endsWith('vuilnis')) {
            reply = "Vergeet niet het glas of het karton te verwerken!";
        }
        else {   
		    reply = "Jij valt niet te redden";
        }
    }
    else if (txt.includes('plaatje')) {
        reply = "";
    }
    else if (txt.includes('mopping') && txt.includes("who")) {
        if (d.date().toDateString() == new Date().toDateString()) {
            reply = dweilen(d.date()) + " should mop";
        }
        else {
            reply = d.text + " should " + dweilen(d.date()) + " mop";
        }
    }
    else if (txt.includes('kong')) {
        reply = "🍌";
    }
    else if (txt.includes('toekabaap')) {
        reply = "kief kief";
    }
    else { // assume status/progress report
        reply = progressTable();
    }

    var footer = '';
    if (Math.random() > 0.14) {
        footer = "\n";
        footer += choice(["- liefs, rob", "- rob 💖", "- je boy (Rob Geus)", 
        // "💝 Mr. Geus", "- Alles wel zo schoon, Rob Geus 💦", "- papa rob 💋",
        // "- Straks, _Red Mijn Vakantie!_, op RTL5", "- jullie huisbitch",
        // "De _Smaakpolitie_ straks bij jou in de straat? Mail naar rtlz@rtl.nl",
        "De ce a roșit roșia? Pentru că a văzut dressingul de salată! - Rob Geŭs",
        `Дзвонить чоловік у ЖЕК:
        — Алло, у мене в квартирі миша!
        — Ну й що?
        — Як що? Вона вже борщ просить! – Роб Геус`,
        "- Kijk nu het nieuwe seizoen van _De Smaakpolitie_ op SBS6, 19:30", "- love, Rob 💘", "- cariños, tu chico Rob Gues", "Cleaning is my life and passion!"]);
    }

    return header + reply + footer;
}


function test() {
    var sender = "liza"; // TODO turn into commandline par?
    var readline = require('readline');
    var rl = readline.createInterface(process.stdin, process.stdout);

    rl.setPrompt('rob> ');
    rl.prompt();

    rl.on('line', function(a) {
        const r = roblogic(a.trim().toLowerCase(), sender);
        console.log(r);
        rl.prompt();
    }).on('close', function() {
         process.exit(0);
    });
}

function statsWeekArray () {
    // {week#: {u1:0/1, ..., dweilen:0/1 }, week#+1: ...}
    var stats_week = new Object() // dictionary
    for (var u of users) {
        stats_week[u] = 0;
    }
    stats_week["dweilen"] = 0;
    return stats_week;
}

function writeStats(s) {
    fs.writeFileSync(stats_path, JSON.stringify(s, 0, 4));
}

function readStats() {
    const d = new Date()
    const w = d.getWeek().toString()
    var s;
    // check if file exists
    if(!fs.existsSync(stats_path) ) {
        const s = {[w] : statsWeekArray()}
        writeStats(s);
        return s 
    }
    s = JSON.parse(fs.readFileSync(stats_path, 'utf8'));
    // new week
    if (!(w in s)) {
        s[w] = statsWeekArray();
        // check if dweilen has already been done this month
        s[w]["dweilen"] = dweilenAlreadyDone(s, w);
        writeStats(s);
    }
    return s
}

function dweilenAlreadyDone (stats, w) {
    // what month was it last week? 
    const lastmonth = chrono.en.parse("sunday last week")[0].date().getMonth()
    // what month is it this week?
    const month = new Date().getMonth();

    if (month == lastmonth && stats[w-1]["dweilen"] == 1) {
        return 1;
    }
    else {
        return 0;
    }
}

function printStats() {
    const total = Object.keys(stats).length
    const row = "*%s*: %s/%s %s\n"
    var o = ""
    for (var u of users) {
        var i = 0; 
        for (var w in stats) {
            i += stats[w][u]
        }
        var n = u[0].toUpperCase()+u.slice(1);
        var e = i == total ? "✨" : "💩"
        o += util.format(row, n, i, total, e)
    }
    return o
}

function updateStats (field) {
    // field can be user or "dweilen"
    const d = new Date()
    const w = d.getWeek().toString()
    
    // stats is a global
    if (!(w in stats)) {
        stats[w] = statsWeekArray();
        // check if dweilen has already been done this month
        stats[w]["dweilen"] = dweilenAlreadyDone(stats, w);
        writeStats(stats);
    }
    
    if (field === "dweilen") {
        const dweilen_done = stats[w]["dweilen"]
        if (dweilen_done) { // dweilen is already done
            return false
        }
        stats[w]["dweilen"] += 1
        writeStats(stats)
        return true
    }
    if (stats[w][field] < 1) {
        stats[w][field] += 1
        writeStats(stats)
        return true;
    }
    else {
        return false;
    }
}


const optionDefinitions = [
  { name: 'verbose', alias: 'v', type: Boolean },
  { name: 'test', alias: 't', type: Boolean },
  { name: 'help', alias: 'h', type: Boolean }
]

const commandLineArgs = require('command-line-args')
const options = commandLineArgs(optionDefinitions)

var stats = readStats();
if (options.help) {
    console.log(`usage: rob.js [-t|--test] \n
-t:	Run rob in offline mode
-h:	Generate this help message`)
    process.exit(0)
}
else if (options.test) {
    test();
}
else {
    var cmd=require('node-cmd');
    const qrcode = require('qrcode-terminal');

    const { Client, LocalAuth } = require('whatsapp-web.js');


    // check out this "rewrite" (?) if you run into problems
    // https://github.com/pedroslopez/whatsapp-web.js/pull/2816
    const wwebVersion = '2.2409.0';

    const client = new Client({
        authStrategy: new LocalAuth(), // your authstrategy here
        puppeteer: {
            // puppeteer args here
        },
        // locking the wweb version<c
        webVersionCache: {
            type: 'remote',
            remotePath: `https://raw.githubusercontent.com/wppconnect-team/wa-version/main/html/${wwebVersion}.html`,
        },
    });

    // const client = new Client({ 
    //     authStrategy: new LocalAuth()
    // });
    
    // needed to send images
    const { MessageMedia } = require('whatsapp-web.js');

    console.log("Authenticating...")
    client.on('authenticated', () => {
        console.log('Authenticated');
    });

    client.on('auth_failure', msg => {
        // Fired if session restore was unsuccessfull
        console.error('Authentication failure', msg);
    });

    client.on('qr', qr => {
        qrcode.generate(qr, {small: true});
    });

    client.on('ready', () => {
        console.log('Client is ready!');
    });


    client.on('message_create', async msg => {
        const txt = msg.body.toLowerCase();
        const contact = await msg.getContact();

        if (txt.includes('@rob')) {
            var sender;
            if (msg.fromMe) {
                sender = "rijnder"
            }
            else {
                const contact = await msg.getContact();
                sender = contact.name;
                if (contact.name === undefined) {
                    sender = contact.pushname.split(" ")[0];
                }
            }
            const r = roblogic(txt, sender.toLowerCase());
            msg.reply(r);
            if (Math.random() > 0.2 || txt.includes("plaatje")) {
                cmd.runSync("./newrob.py");
                const media = MessageMedia.fromFilePath("./r.jpg");
                const c = await msg.getChat();
                c.sendMessage(media);
            }
        }
    });

    client.initialize().catch(console.log);
}
